<?php require 'PHPMailer/PHPMailerAutoload.php';
include_once 'MailChimp.php';
session_start();

$json = file_get_contents('php://input');
$post = json_decode($json);
if (isset($post->email) && isset($post->cart) && spamcheck($post->email) && $_SESSION['key'] == $post->key) {
	unset($_SESSION['key']);
	if ($post->info) {
		$MailChimp = new \Drewm\MailChimp('cf6af8b47d24363005bd35d3357af455-us9');
		$result = $MailChimp->call('lists/subscribe', array(
			'id' => 'c365b10bca',
			'email' => array('email'=>$post->email),
			'double_optin' => false,
			'update_existing' => true,
			'replace_interests' => false,
			'send_welcome' => false,
		));
	}
	$cartItems = json_decode($post->cart);
	if (count($cartItems) == 0) {
		die('{"error": "La lista è vuota."}');
	}
	$unsubscribeUrl = 'http://'.$_SERVER['SERVER_NAME'].'/unsubscribe.php?e='.urlencode($post->email);
	$htmlMessage = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>La tua lista della spesa</title>
</head>
<body>
<table style="background-color:#fff;font-family:Arial,Helvetica,sans-serif" width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="550" height="94"><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/logo.jpg" width="550" height="94" alt="Esselunga®" style="display: block;"></td>
</tr>
</table>
<table width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="550" height="66"><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/payoff.jpg" width="550" height="66" alt="SE AMI LA CONVENIENZA, VIENI A FARE IL PIENO" style="display: block;"></td>
</tr>
</table>
<table width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="550" height="102"><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/testo_grazie.jpg" width="550" height="102" alt="Grazie per aver utilizzato il nostro volantino interattivo.
Qui sotto troverai tutti i prodotti* che hai selezionato sul sito 
esselungatifailpieno.it e lo sconto carburante che potrai 
accumulare con la tua spesa." style="display: block;"></td>
</tr>
</table>
<table width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="550" height="59"><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/titolo_lista.jpg" width="550" height="59" alt="La tua lista della spesa" style="display: block;"></td>
</tr>
</table>
<table width="480" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#dedede;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:11px;color:#000;text-align:left">
<tr>
<td style="border-right:solid;border-right-color:#fff;border-right-width:1px;width:71px;text-align:center" width="71" height="32">Quantità</td>
<td style="background-color:#dedede;border-right:solid;border-right-color:#fff;border-right-width:1px;width:292px;padding-left:10px" width="292" height="32">Descrizione prodotto</td>
<td style="background-color:#dedede;width:107px" width="107" height="32">Sconto carburante</td>
</tr>
</table>';
	setlocale(LC_MONETARY, 'it_IT.UTF-8');
	$sum = 0;
	foreach ($cartItems as $item) {
		$price = floatval($item->price);
		$total = $price * $item->quantity;
		if ($item->discount && $item->quantity > 1) {
			$total += floor($item->quantity / 2) * $price;
		}
		if ($item->discount) {
			$htmlMessage .= '<table width="480" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#dedede;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:11px;color:#000;border-top:dotted;border-top-color:#eaeaea;border-top-width:1px;display:block">
	<tr>
		<td style="border-right:solid;border-right-color:#fff;border-right-width:1px;width:71px;text-align:center;padding-bottom:10px">'.$item->quantity.'</td>
		<td style="background-color:#fffdba;border-right:solid;border-right-color:#fff;border-right-width:1px;width:292px;padding-left:10px;padding-bottom:10px" width="292">
			<table style="border-right:dotted;border-right-color:#eaeaea;border-right-width:1px;display:block" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="font-weight:bold;font-size:13px;padding-bottom:5px;padding-top:5px">'.$item->name.'</td>
				</tr>
				<tr>
					<td style="font-size:11px;color:#999;padding-bottom:10px">'.$item->description.'</td>
				</tr>
			</table>
		</td>
		<td style="background-color:#fffdba;width:107px;text-align:center" width="107">
			<table border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-bottom:5px"><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/triplica.png" width="68" height="26" alt="triplica lo sconto" style="display: block;"></td>
				</tr>
				<tr>
					<td style="font-size:25px;color:#e40514;padding-bottom:10px;text-align:center">'.money_format('%.2n', $total).'</td>
				</tr>
			</table>
		</td>
	</tr>
</table>';
		} else {
			$htmlMessage .= '<table width="480" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#dedede;font-family:Arial,Helvetica,sans-serif;font-weight:bold;font-size:11px;color:#000;border-top:dotted;border-top-color:#eaeaea;border-top-width:1px;display:block">
	<tr>
		<td style="border-right:solid;border-right-color:#fff;border-right-width:1px;width:71px;text-align:center;padding-bottom:10px">'.$item->quantity.'</td>
		<td style="background-color:#fff;border-right:solid;border-right-color:#fff;border-right-width:1px;width:292px;padding-left:10px;padding-bottom:10px" width="292">
			<table style="border-right:dotted;border-right-color:#eaeaea;border-right-width:1px;display:block" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td style="font-weight:bold;font-size:13px;padding-bottom:5px;padding-top:5px">'.$item->name.'</td>
				</tr>
				<tr>
					<td style="font-size:11px;color:#999;padding-bottom:10px">'.$item->description.'</td>
				</tr>
			</table>
		</td>
		<td style="background-color:#fff;width:107px;text-align:center" width="107">
			<table border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td style="padding-bottom:5px;padding-top:5px"></td>
				</tr>
				<tr>
					<td style="font-size:25px;color:#e40514;padding-bottom:10px;text-align:center">'.money_format('%.2n', $total).'</td>
				</tr>
			</table>
		</td>
	</tr>
</table>';
		}
		$sum += $total;
	}
	$htmlMessage .= '<table width="480" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#345294">
<tr style="font-size:18px">
<td style="color:#fff;text-align:left;font-weight:bold;padding:5px;line-height:48px" width="372" height="48">TOTALE SCONTO CARBURANTE</td>
<td width="107" height="48" align="center" valign="middle" style="color:#e40614;font-weight:bold;font-size:24px">'.money_format('%.2n', $sum).'</td>
</tr>
</table>
<table width="480" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/asterisco.jpg" width="480" height="79" alt="*Prodotti in promozione dall’11 al 24 settembre. Il numero di prodotti in promozione potrebbe variare in relazione
alla dimensione del negozio " style="display: block;"></td>
</tr>
</table>
<table width="480" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td width="480" height="113"><a href="http://esselungatifailpieno.it" target="_blank"><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/vai_al_sito.jpg" border="0" width="480" height="113" alt="Scopri come utilizzare i buoni sconto" style="display: block;"></a></td>
</tr>
</table>
<table width="480" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td width="480" height="108"><a href="http://esselungatifailpieno.it/mappa.html" target="_blank"><img src="http://'.$_SERVER['SERVER_NAME'].'/images/email/apri_mappa.jpg" border="0" width="480" height="108" alt="Scopri i negozi Esselunga più vicini 
a te e tutte le stazioni di servizio Q8 
e Shell che partecipano all&#39;iniziativa. Vai alla mappa" style="display: block;"></a></td>
</tr>
</table>
<table style="font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:center;color:#000;padding:20px" width="550" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Ti abbiamo inviato questa email in quanto registrato al sito <a href="http://esselungatifailpieno.it" target="_blank">esselungatifailpieno.it</a>. 
Se non ti sei registrato al sito qualcuno potrebbe aver inserito il tuo indirizzo email per sbaglio.</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>';

	$textMessage = 'Grazie per aver utilizzato il nostro volantino interattivo.
Qui sotto troverai tutti i prodotti che hai selezionato sul sito
esselungatifailpieno.it e lo sconto carburante che potrai
accumulare con la tua spesa.

----------------------------------
La tua lista della spesa:
----------------------------------';
	setlocale(LC_MONETARY, 'it_IT.UTF-8');
	$sum = 0;
	foreach ($cartItems as $item) {
		$price = floatval($item->price);
		$total = $price * $item->quantity;
		if ($item->discount && $item->quantity > 1) {
			$total += floor($item->quantity / 2) * $price;
		}
		$textMessage .= "\n".'- '.$item->quantity.' '.$item->name.', '.$item->description."\n".'Sconto carburante: '.money_format('%.2n', $total)."\n";
		$sum += $total;
	}
	$textMessage .= '----------------------------------
TOTALE SCONTO CARBURANTE: '.money_format('%.2n', $sum).'
----------------------------------

Scopri come utilizzare i buoni sconto:
http://esselungatifailpieno.it

Scopri i negozi Esselunga più vicini a te e tutte le
stazioni di servizio Q8 e Shell che partecipano all\'iniziativa:
http://esselungatifailpieno.it/mappa.html


Esselunga® - Se ami la convenienza, vieni a fare il pieno

Ti abbiamo inviato questa email in quanto registrato al sito www.esselungatifailpieno.it.
Se non ti sei registrato al sito qualcuno potrebbe aver inserito il tuo indirizzo email per sbaglio.
Se non vuoi più ricevere questa newsletter, visita questo indirizzo: '.$unsubscribeUrl;
	
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->Host = 'mail.esselungatifailpieno.it';
	$mail->SMTPAuth = false;
	$mail->From = 'noreply@esselungatifailpieno.it';
	$mail->FromName = 'Esselunga';
	$mail->addAddress($post->email);
	$mail->addReplyTo('noreply@esselungatifailpieno.it', 'Esselunga');
	$mail->ReturnPath = 'noreply@esselungatifailpieno.it';
	$mail->CharSet = 'UTF-8';
	$mail->WordWrap = 70;
	$mail->isHTML(true);
	$mail->Subject = 'La tua lista della spesa';
	$mail->Body = $htmlMessage;
	$mail->AltBody = $textMessage;

	if ($mail->send()) {
		die('{"success": "La lista è stata inviata."}');
	} else {
		die('{"error": "Impossibile inviare la lista, riprovare più tardi."}');
	}
}
header('Location: http://'.$_SERVER['SERVER_NAME'].'/notfound.html');

function spamcheck($field) {
	$field = filter_var($field, FILTER_SANITIZE_EMAIL);
	if (filter_var($field, FILTER_VALIDATE_EMAIL)) {
		return true;
	} else {
		return false;
	}
}
/*
function money_format($format, $number)
{
	$regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.
			'(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
// 	if (setlocale(LC_MONETARY, 0) == 'C') {
// 		setlocale(LC_MONETARY, '');
// 	}
	setlocale(LC_MONETARY, 'it_IT.UTF-8');
	$locale = localeconv();
	preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
	foreach ($matches as $fmatch) {
		$value = floatval($number);
		$flags = array(
				'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
				$match[1] : ' ',
				'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
				'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
				$match[0] : '+',
				'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
				'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
		);
		$width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
		$left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
		$right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
		$conversion = $fmatch[5];

		$positive = true;
		if ($value < 0) {
			$positive = false;
			$value  *= -1;
		}
		$letter = $positive ? 'p' : 'n';

		$prefix = $suffix = $cprefix = $csuffix = $signal = '';

		$signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
		switch (true) {
			case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
				$prefix = $signal;
				break;
			case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
				$suffix = $signal;
				break;
			case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
				$cprefix = $signal;
				break;
			case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
				$csuffix = $signal;
				break;
			case $flags['usesignal'] == '(':
			case $locale["{$letter}_sign_posn"] == 0:
				$prefix = '(';
				$suffix = ')';
				break;
		}
		if (!$flags['nosimbol']) {
			$currency = $cprefix .
			($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
			$csuffix;
		} else {
			$currency = '';
		}
		$space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';

		$value = number_format($value, $right, $locale['mon_decimal_point'],
				$flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
		$value = @explode($locale['mon_decimal_point'], $value);

		$n = strlen($prefix) + strlen($currency) + strlen($value[0]);
		if ($left > 0 && $left > $n) {
			$value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
		}
		$value = implode($locale['mon_decimal_point'], $value);
		if ($locale["{$letter}_cs_precedes"]) {
			$value = $prefix . $currency . $space . $value . $suffix;
		} else {
			$value = $prefix . $value . $space . $currency . $suffix;
		}
		if ($width > 0) {
			$value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
					STR_PAD_RIGHT : STR_PAD_LEFT);
		}

		$format = str_replace($fmatch[0], $value, $format);
	}
	return $format;
}
*/