angular.module('esselungaStore', ['ngRoute', 'psResponsive', 'ngDraggable', 'ngAnimate', 'ngAnimate-animate.css', 'angularModalService', 'angulartics', 'angulartics.google.analytics']).

	config(['$routeProvider', '$analyticsProvider', function($routeProvider, $analyticsProvider) {
		$routeProvider.
			when('/store', {
				templateUrl: 'partials/store.html',
				controller: 'storeController'
			}).
			when('/store/cart', {
				templateUrl: 'partials/cart.html',
				controller: 'cartController'
			}).
			otherwise({
				redirectTo: '/store'
			});
	}]).
	
	controller('storeController', ['$scope', '$http', 'psResponsive', 'storeService', 'ModalService', function($scope, $http, psResponsive, storeService, ModalService) {
		$scope.store = storeService.store;
		$scope.priceRanges = [
   		    {from: '0.2', to: '5', label: 'Mostra tutti gli sconti'},
		    {from: '0.2', to: '0.5', label: 'Da 0,20€ a 0,50€'},
		    {from: '0.6', to: '0.8', label: 'Da 0,60€ a 0,80€'},
		    {from: '0.9', to: '1.4', label: 'Da 0,90€ a 1,40€'},
		    {from: '1.6', to: '5', label: 'Da 1,60€ a 5,00€'}
		];
		$scope.priceRange = 0;
		$scope.priceFrom = parseFloat($scope.priceRanges[0].from);
		$scope.priceTo = parseFloat($scope.priceRanges[$scope.priceRanges.length - 1].to);
		$scope.changeRange = function() {
			var selectedRange = $scope.priceRanges[$scope.priceRange];
			$scope.priceFrom = parseFloat(selectedRange.from);
			$scope.priceTo = parseFloat(selectedRange.to);
		};
		$scope.categories = [
   		    {value: 'ALIMENTARI', label: 'Alimentari'},
   		    {value: 'BIBITE E BEVANDE', label: 'Bibite e bevande'},
   		    {value: 'CURA PERSONA', label: 'Cura persona'},
   		    {value: 'CURA CASA', label: 'Cura casa'},
   		    {value: 'CURA ANIMALI', label: 'Cura animali'},
   		    {value: 'FRESCHISSIMI', label: 'Freschissimi'}
		];
		$scope.cart = storeService.cart;
		$scope.responsive = psResponsive;
		$scope.onDropComplete = function(product) {
			storeService.cart.addItem(product, 1);
		};
	    $scope.sendCart = function() {
	        ModalService.showModal({
	            templateUrl: 'partials/modal-sendcart.php',
	            controller: 'sendCartController'
	        }).then(function(modal) {
	            modal.element.modal({backdrop: 'static'});
	        });
	    };
	    var unregister = $scope.$watch('cart.items', function(newItems, oldItems) {
	    	if ((newItems.length == oldItems.length + 1) && newItems[newItems.length - 1].discount) {
		        ModalService.showModal({
		            templateUrl: 'partials/modal-x3.php',
		            controller: 'x3Controller'
		        }).then(function(modal) {
		            modal.element.modal();
		        });
	    		unregister();
	    	}
	    }, true);
	    $http.get('products.json').success(function(data) {
			storeService.store.products = data;
			storeService.cart.checkItems(data);
		});
	}]).
	
	controller('cartController', ['$scope', '$window', 'storeService', 'ModalService', function($scope, $window, storeService, ModalService) {
		$scope.cart = storeService.cart;
	    $scope.sendCart = function() {
	        ModalService.showModal({
	            templateUrl: 'partials/modal-sendcart.php',
	            controller: 'sendCartController'
	        }).then(function(modal) {
	            modal.element.modal({backdrop: 'static'});
	        });
	    };
		angular.element($window).on('resize', function(e) {
			if ($window.innerWidth > 767 && $window.location.hash.indexOf('#/store/cart') != -1) {
				$window.location.hash = '#/store';
			}
		});
	}]).
	
	controller('sendCartController', ['$scope', '$http', '$analytics', 'storeService', function($scope, $http, $analytics, storeService) {
		$scope.master = {};
		$scope.submit = function(form) {
			$analytics.pageTrack('/sendcart');
			$scope.master = angular.copy(form);
        	$scope.successMessage = $scope.errorMessage = null;
    		$http.post('sendcart.php', {email: form.email, info: form.info, cart: angular.toJson(storeService.cart.items), key: form.key})
    			.success(function(data) {
    				if (data.success) {
                		$scope.successMessage = data.success;
    				} else if (data.error) {
                		$scope.errorMessage = data.error;
    				} else {
            			$scope.errorMessage = 'Impossibile inviare la lista, riprovare più tardi.';
    				}
        		})
        		.error(function(data) {
        			$scope.errorMessage = 'Impossibile inviare la lista, riprovare più tardi.';
        		});
		};
		$scope.reset = function() {
			$scope.form = angular.copy($scope.master);
		};
		$scope.isUnchanged = function(form) {
			return angular.equals(form, $scope.master);
		};
		$scope.reset();
	}]).
	
	controller('x3Controller', function($scope) {}).
	
	filter('priceFilter', function() {
	    return function(items, scope) {
	        var filtered = [];
	        angular.forEach(items, function(item) {
	            if (item.price >= scope.priceFrom && item.price <= scope.priceTo) {
	                filtered.push(item);
	            }
	        });
	        return filtered;
	    };
	}).

	directive('ssProduct', function() {
		return {
			restrict: 'E',
			scope: { storeProduct: '=product' },
			templateUrl: 'partials/ss-product.html'
		};
	}).

	directive('animateOnChange', function($animate) {
		return function(scope, elem, attr) {
			scope.$watch(attr.animateOnChange, function(newValue, oldValue) {
				if (newValue != oldValue) {
					$animate.addClass(elem, 'update', function() {
						$animate.removeClass(elem, 'update');
					});
				}
			})  
		}
	}).
	
	directive('nullIsUndefined', function() {
	    return {
	        restrict: 'A',
	        require: 'ngModel',
	        link: function postLink(scope, elem, attrs, modelCtrl) {
	            modelCtrl.$parsers.push(function (newViewValue) {
	                if (newViewValue === null) {
	                    newViewValue = undefined;
	                }
	                return newViewValue;
	            });
	        }
	    };
	}).

	factory('storeService', function () {
	    return {
	        store: new Store(),
	        cart: new Cart('esselungaStore')
	    };
	});
