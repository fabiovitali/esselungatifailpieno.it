﻿function Cart(cartName)
{
    this.cartName = cartName;
    this.clearCart = false;
    this.items = [];

    // load items from local storage when initializing
    this.loadItems();

    // save items to local storage when unloading
    var self = this;
    angular.element(window).on('unload', function () {
        if (self.clearCart) {
            self.clearItems();
        }
        self.saveItems();
        self.clearCart = false;
    });
}

Cart.prototype.loadItems = function () {
    var items = localStorage != null ? localStorage[this.cartName + "_items"] : null;
    if (items != null) {
        try {
            var items = angular.fromJson(items);
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.id != null && item.name != null && item.description != null && item.price != null && item.category != null && item.image != null && item.discount != null && item.quantity != null) {
                    item = new cartItem(item.id, item.name, item.description, item.category, item.price, item.image, item.discount, item.quantity);
                    this.items.push(item);
                }
            }
        }
        catch (err) {
            // ignore errors while loading...
        }
    }
}

Cart.prototype.saveItems = function () {
    if (localStorage != null) {
        localStorage[this.cartName + "_items"] = angular.toJson(this.items);
    }
}

Cart.prototype.addItem = function (product, quantity) {
    quantity = this.toNumber(quantity);
    if (quantity != 0) {

        // update quantity for existing item
        var found = false;
        for (var i = 0; i < this.items.length && !found; i++) {
            var item = this.items[i];
            if (item.id == product.id) {
                found = true;
                item.quantity = this.toNumber(item.quantity + quantity);
                if (item.quantity <= 0) {
                    this.items.splice(i, 1);
                }
            }
        }

        // new item, add now
        if (!found) {
            var item = new cartItem(product.id, product.name, product.description, product.category, product.price, product.image, product.discount, quantity);
            this.items.push(item);
        }

        // save changes
        this.saveItems();
    }
}

Cart.prototype.removeItem = function (id) {
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        if (item.id == id) {
        	if (item.quantity > 1) {
        		item.quantity--;
        	} else {
        		this.items.splice(i, 1);
        	}
        	break;
        }
    }
    this.saveItems();
}

Cart.prototype.checkItems = function (products) {
	var i = this.items.length;
	while (i--) {
		var item = this.items[i];
		if (!this.itemExists(item, products)) {
			this.items.splice(i, 1);
		}
	}
    this.saveItems();
}

Cart.prototype.itemExists = function (item, products) {
    for (var i = 0; i < products.length; i++) {
    	var product = products[i];
    	if (item.id == product.id) {
    		return true;
    	}
    }
	return false;
}

Cart.prototype.getTotalPrice = function (id) {
    var total = 0;
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        if (id == null || item.id == id) {
        	if (item.discount && item.quantity > 1) {
       			total += this.toNumber(Math.floor(item.quantity / 2) * item.price);
       		}
       		total += this.toNumber(item.quantity * item.price);
        }
    }
    return total;
}

Cart.prototype.getTotalCount = function (id) {
    var count = 0;
    for (var i = 0; i < this.items.length; i++) {
        var item = this.items[i];
        if (id == null || item.id == id) {
            count += this.toNumber(item.quantity);
        }
    }
    return count;
}

Cart.prototype.clearItems = function () {
    this.items = [];
    this.saveItems();
}

Cart.prototype.toNumber = function (value) {
    value = value * 1;
    return isNaN(value) ? 0 : value;
}

function cartItem(id, name, description, category, price, image, discount, quantity) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.category = category;
    this.price = price * 1;
    this.image = image;
    this.discount = discount;
    this.quantity = quantity * 1;
}

