<?php include_once 'MailChimp.php';

if (isset($_GET['e'])) {
	$email = html_entity_decode($_GET['e']);
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$MailChimp = new \Drewm\MailChimp('cf6af8b47d24363005bd35d3357af455-us9');
		$result = $MailChimp->call('lists/unsubscribe', array(
			'id' => 'c365b10bca',
			'email' => array('email'=>$email),
			'send_goodbye' => false,
			'send_notify' => false,
		));
		if ($result['complete']) {
			header('Location: http://'.$_SERVER['SERVER_NAME'].'/grazie.html');
			exit;
		}
	}
}
header('Location: http://'.$_SERVER['SERVER_NAME'].'/notfound.html');
