<?php session_start();
$_SESSION['key'] = uniqid($_SERVER['SERVER_NAME']); ?>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sendCart" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" ng-controller="sendCartController" ng-init="form.key = '<?php echo $_SESSION['key'] ?>'">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" ng-click="close('Cancel')">
					<span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span>
				</button>
				<h4 class="modal-title" id="sendCart">Vuoi ricevere la tua selezione di prodotti in volantino direttamente alla tua casella e-mail?</h4>
			</div>
			<div class="modal-body">
				<div class="panel-body">
					<div ng-show="successMessage" class="alert alert-success" role="alert">{{successMessage}}</div>
					<div ng-show="errorMessage" class="alert alert-danger" role="alert">{{errorMessage}}</div>
					<form name="sendCartForm" novalidate role="form">
 						<input type="hidden" name="key" ng-model="form.key"/>
   						<div ng-class="{'has-error': sendCartForm.privacy.$dirty && sendCartForm.privacy.$invalid}">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="privacy" ng-model="form.privacy" required/>
									Ho letto l'informativa sulla <a href="privacy.html" target="_blank">Privacy</a>
								</label>
								<p class="error text-danger" ng-show="sendCartForm.privacy.$dirty && !form.privacy">È necessario accettare la privacy</p>
							</div>
   						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="info" ng-model="form.info"/>
								Autorizzo Esselunga all'invio di informazioni commerciali
							</label>
						</div>
						<div class="form-group" ng-class="{'has-error': sendCartForm.email.$dirty && sendCartForm.email.$invalid}">
							<label class="control-label" for="email">Inserisci qui sotto il tuo indirizzo e autorizza Esselunga a ricontattarti</label>
							<input type="email" class="form-control" placeholder="E-mail" name="email" ng-model="form.email" required/>
							<div ng-show="sendCartForm.email.$dirty && sendCartForm.email.$invalid">
								<p class="error text-danger" ng-show="sendCartForm.email.$error.required">Indirizzo e-mail obbligatorio</p>
								<p class="error text-danger" ng-show="sendCartForm.email.$error.email">Indirizzo e-mail non valido</p>
							</div>
   						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
				<button type="button" class="btn btn-primary" ng-click="submit(form)" ng-disabled="sendCartForm.$invalid || isUnchanged(form)">Invia</button>
			</div>
		</div>
	</div>
</div>
