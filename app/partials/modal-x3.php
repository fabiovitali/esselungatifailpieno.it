<div class="x3 modal fade" tabindex="-1" role="dialog" aria-labelledby="sendCart" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Chiudi</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="panel-body">
					<h3>Questo prodotto partecipa alla promozione <strong>Triplica lo sconto</strong></h3>
					<p>Ogni due confezioni di prodotto lo sconto carburante Q8 si triplica. Inserisci un altro prodotto nel carrello e continua ad accumulare il tuo sconto!</p>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
