<?php include_once 'MailChimp.php';
require 'PHPMailer/PHPMailerAutoload.php';
session_start();

$json = file_get_contents('php://input');
$post = json_decode($json);
if (isset($post->email) && spamcheck($post->email) && isset($_SESSION['key']) && $_SESSION['key'] == $post->key) {
	unset($_SESSION['key']);
	$MailChimp = new \Drewm\MailChimp('cf6af8b47d24363005bd35d3357af455-us9');
	$result = $MailChimp->call('lists/subscribe', array(
		'id' => 'c365b10bca',
		'email' => array('email'=>$post->email),
		'double_optin' => false,
		'update_existing' => true,
		'replace_interests' => false,
		'send_welcome' => false,
	));
	if (isset($result['email'])) {
		sendConfirmation($post->email);
		die('{"success": "Grazie per esserti registrato! Nei prossimi giorni ti manderemo una e-mail con tutte le novità sull\'iniziativa Esselunga."}');
	} else {
		die('{"error": "Impossibile registrarsi, riprovare più tardi"}');
	}
}
header('Location: http://'.$_SERVER['SERVER_NAME'].'/notfound.html');

function sendConfirmation($to) {
	$subject = "Conferma registrazione al sito esselungatifailpieno.it";
	$unsubscribeUrl = 'http://'.$_SERVER['SERVER_NAME'].'/unsubscribe.php?e='.urlencode($to);
	$htmlMessage = '<p>Grazie per esserti registrato al sito esselungatifailpieno.it.</p>
<p>Stiamo preparando una sorpresa per te e per tutta la tua famiglia. A breve ti manderemo una e-mail a questo indirizzo con tutte le novità Esselunga.</p>
<p><strong>Sei pronto a partire?</strong></p>
<br/>
<p><em>Esselunga® - Se ami la convenienza, vieni a fare il pieno</em></p>
<br/>
<p>Ti abbiamo inviato questa e-mail in quanto registrato al sito <a href="http://esselungatifailpieno.it" target="_blank">esselungatifailpieno.it</a>. Se non ti sei registrato al sito qualcuno potrebbe aver inserito il tuo indirizzo e-mail per sbaglio. Se non vuoi più ricevere questa newsletter, <a href="'.$unsubscribeUrl.'">clicca qui</a></p>';
	$textMessage = 'Grazie per esserti registrato al sito esselungatifailpieno.it.

Stiamo preparando una sorpresa per te e per tutta la tua famiglia.
A breve ti manderemo una e-mail a questo indirizzo con tutte le novità Esselunga.

Sei pronto a partire?


Esselunga® - Se ami la convenienza, vieni a fare il pieno

Ti abbiamo inviato questa e-mail in quanto registrato al sito esselungatifailpieno.it.
Se non ti sei registrato al sito qualcuno potrebbe aver inserito il tuo indirizzo e-mail per sbaglio.
Se non vuoi più ricevere questa newsletter, visita questo link: '.$unsubscribeUrl;
	
	$mail = new PHPMailer;
	$mail->isSMTP();
	$mail->Host = 'mail.esselungatifailpieno.it';
	$mail->SMTPAuth = false;
	$mail->From = 'noreply@esselungatifailpieno.it';
	$mail->FromName = 'Esselunga';
	$mail->addAddress($to);
	$mail->addReplyTo('noreply@esselungatifailpieno.it', 'Esselunga');
	$mail->ReturnPath = 'noreply@esselungatifailpieno.it';
	$mail->CharSet = 'UTF-8';
	$mail->WordWrap = 70;
	$mail->isHTML(true);
	$mail->Subject = 'Grazie per esserti registrato';
	$mail->Body = $htmlMessage;
	$mail->AltBody = $textMessage;

	return $mail->send();
}

function spamcheck($field) {
	$field = filter_var($field, FILTER_SANITIZE_EMAIL);
	if (filter_var($field, FILTER_VALIDATE_EMAIL)) {
		return true;
	} else {
		return false;
	}
}
